import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { GetListTypeComponent } from './get-list-type/get-list-type.component';
import { ProcessTypeComponent } from './process-type/process-type.component';

export const routes: Routes = [

    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: GetListTypeComponent },
    { path: 'insert', component: ProcessTypeComponent },
    { path: 'update/:id', component: ProcessTypeComponent },
];
@NgModule({
    declarations: [
        GetListTypeComponent,
        ProcessTypeComponent,],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        AlertModule.forRoot()
    ]
})
export class SetiingsPagesModule { }
