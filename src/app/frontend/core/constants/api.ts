// App Component
export const GET_COMPANY_INFO = 'api/company'
export const GetCompanyInfo = '@Company/GetCompanyInfo'

// Home Page
export const GET_PRODUCTS_PIN = 'api/getProductsPin'
export const GetProductsPin = '@Products/GetAllPin'

// Pages
export const GET_CHILDREN_PAGES = 'api/pages/getChildrenPages'
export const GetChildrenPages = '@Pages/GetAllChildren'
export const PAGES_GET_NEW_PRODUCTS = 'api/pages/getNewProducts'
export const PagesGetNewProducts = '@Products/GetNew'
export const PAGES_GET_HOT_NEWS = 'api/pages/getHotNews'
export const PagesGetHotNews = '@News/GetHot'
export const GET_PARTNERS_PIN = 'api/pages/getPartnersPin'
export const GetPartnersPin = '@Partners/GetAllPin'
export const PAGES_GET_BANNER = 'api/pages/getBanner'
export const PagesGetBanner = '@Banner/GetBanner'
