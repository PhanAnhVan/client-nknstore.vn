import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Globals } from 'src/app/globals';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],

})
export class MenuComponent implements OnInit, OnDestroy {
    private connect: Subscription;
    public token: any = {
        menu: "api/getmenu",
    }

    public menu: any;
    public active: number = 0;
    public link: string = ''
    public sub_menu: boolean = true;
    public width: number = window.innerWidth;

    constructor(
        public globals: Globals,
        public router: Router,
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getMenuMain":
                    let data = this.compaid(res.data);
                    this.menu = data;
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {

        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {

                this.sub_menu = false
            }
        });
        if (this.width > 1024) {
            this.globals.send({ path: this.token.menu, token: 'getMenuMain', params: { position: 'menuMain' } });
        }
    }

    compaid(data: any[]) {
        let list = [];

        data = data.filter(function (item: { parent_id: string | number; special: number; }) {

            let v = (isNaN(+item.parent_id) || item.special == 1) ? 0 : +item.parent_id;

            v == 0 ? '' : list.push(item);

            return v == 0 ? true : false;
        });

        let compaidmenu = (data: string | any[], skip: boolean, level = 0) => {

            level = level + 1;

            if (skip == true) {

                return data;

            } else {

                for (let i = 0; i < data.length; i++) {

                    let obj = [];

                    list = list.filter(item => {

                        let skip = (+item.parent_id == +data[i]['id']) ? false : true;

                        if (skip == false) { obj.push(item); }

                        return skip;

                    })

                    let skip = (obj.length == 0) ? true : false;

                    data[i]['href'] = getType(data[i]['link'], +data[i].type);

                    data[i]['level'] = level;

                    data[i]['data'] = compaidmenu(obj, skip, level);

                }

                return data;

            }

        };
        let getType = (link: string, type: number) => {
            switch (+type) {
                case 1:
                case 2:
                case 4:
                    link = link
                    break;
                case 3:
                    link = 'san-pham/' + link
                    break;
                default:
                    break;
            }
            return link;
        }
        return compaidmenu(data, false);

    }

    // clickOutside close menu here.
    clickOutside() {
        const el = document.getElementById('sub-menu');

        if (el) {
            el.style.setProperty('display', 'none');

            return false;
        }
    }

    @Output('menumobile') menumobile = new EventEmitter<number>();

    showMenuChild = (skip: any, item: { data: string | any[]; id: string; type: string | number; href: string; }) => {

        if (item.data && item.data.length > 0) {

            let elm = document.getElementById('dropdown-menu-child-' + item.id);

            skip ? elm.classList.add("active-menu-child") : (elm.classList.remove("active-menu-child"))

        } else {

            let elm2 = document.getElementById("menu-mobi");

            elm2.classList.add("menu-mobi-hidden");

            elm2.classList.remove("menu-mobi-block");

            this.menumobile.emit();

            if (skip) {

                let elm = document.getElementsByClassName('menu-child');

                elm[0].classList.remove("active-menu-child")
            }

            +item.type === 1 ? window.open(item.href, "_blank") : this.router.navigate(['/' + item.href]);
        }
    }

    public active_class: string = '';
    public active_level: number = 0;

    onActiveLevel = (id: number) => {
        this.active_level = id;
    }

    routerLink = (item: { data: string | any[]; id: number; href: string; }) => {
        if (item.data && item.data.length > 0) {
            this.active = +this.active == item.id ? 0 : +item.id;
            this.active_class = 'active';
            this.active_level = item.data[0].id;
            this.sub_menu = true
        } else {
            this.active = 0
            this.router.navigate(['/' + item.href]);
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }
}
