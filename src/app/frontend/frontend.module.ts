/*
|--------------------------------------------------------------------------
| CREATE: PHAN ANH VAN
| DATE: 11/10/2021
|--------------------------------------------------------------------------
*/
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { LazyLoadImageModule } from 'ng-lazyload-image'
import { CollapseModule } from 'ngx-bootstrap/collapse'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { BsDropdownModule } from 'ngx-bootstrap/dropdown'
import { ModalModule } from 'ngx-bootstrap/modal'
import { PaginationModule } from 'ngx-bootstrap/pagination'
import { TabsModule } from 'ngx-bootstrap/tabs'
import { TimepickerModule } from 'ngx-bootstrap/timepicker'
import { TypeaheadModule } from 'ngx-bootstrap/typeahead'
import { CarouselModule } from 'ngx-owl-carousel-o'
import { BindSrcDirective } from '../services/directive/bindSrc.directive'
import { ClickOutsideDirective } from '../services/directive/clickOutside.directive'
import { ContactComponent } from './contact/contact.component'
import { DetailContentComponent } from './content/detail-content/detail-content.component'
import { ListContentComponent } from './content/list/content.component'
import { FrontendComponent } from './frontend.component'
import { HomeComponent } from './home/home.component'
import { BoxContentGridComponent } from './modules/box-content-grid/box-content-grid.component'
import { BoxContentComponent } from './modules/box-content/box-content.component'
import { BoxProductComponent } from './modules/box-product/box-product.component'
import { CommentComponent } from './modules/comment/comment.component'
import { FooterComponent } from './modules/footer/footer.component'
import { HeaderComponent } from './modules/header/header.component'
import { MenuMobileComponent } from './modules/menu-mobile/menu-mobile.component'
import { MenuComponent } from './modules/menu/menu.component'
import { SlideComponent } from './modules/slide/slide.component'
import { NotFoundComponent } from './not-found/not-found.component'
import { PageComponent } from './page/page.component'
import { DetailProductComponent } from './product/detail-product/detail-product.component'
import { ListProductComponent } from './product/list/product.component'
import { SanitizeHtmlPipe } from './sanitizeHtml.pipe'
import { SearchComponent } from './search/search.component'

const appRoutes: Routes = [
    {
        path: '',
        component: FrontendComponent,
        children: [
            { path: 'trang-chu', redirectTo: '' },
            { path: '', component: HomeComponent },
            { path: 'tin-tuc', component: ListContentComponent },
            { path: 'tin-tuc/:link', component: ListContentComponent },
            { path: 'tin-tuc/:links/:link', component: DetailContentComponent },
            { path: 'san-pham', component: ListProductComponent },
            { path: 'san-pham/:link', component: ListProductComponent },
            { path: 'san-pham/:links/:link', component: DetailProductComponent },
            { path: 'lien-he', component: ContactComponent },
            { path: 'tim-kiem', component: SearchComponent },
            { path: 'tim-kiem/:keywords', component: SearchComponent },
            { path: '404', component: NotFoundComponent },
            { path: ':parent_link/:link', component: PageComponent },
            { path: ':link', component: PageComponent }
        ]
    },
    { path: '**', redirectTo: '404' }
]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        CollapseModule,
        TabsModule.forRoot(),
        RouterModule.forChild(appRoutes),
        TimepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        ModalModule.forRoot(),
        CarouselModule,
        TypeaheadModule.forRoot(),
        PaginationModule.forRoot(),
        BsDropdownModule.forRoot(),
        LazyLoadImageModule
    ],
    declarations: [
        FrontendComponent,
        HomeComponent,
        ContactComponent,
        HeaderComponent,
        FooterComponent,
        PageComponent,
        MenuComponent,
        MenuMobileComponent,
        CommentComponent,
        DetailContentComponent,
        BoxContentComponent,
        BoxContentGridComponent,
        ListContentComponent,
        ListProductComponent,
        SanitizeHtmlPipe,
        BindSrcDirective,
        ClickOutsideDirective,
        SearchComponent,
        DetailProductComponent,
        SlideComponent,
        NotFoundComponent,
        BoxProductComponent
    ]
})
export class FrontendModule {}
