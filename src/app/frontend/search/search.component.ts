import { Component, OnInit, OnDestroy } from '@angular/core'
import { ToslugService } from '../../services/integrated/toslug.service'
import { TableService } from '../../services/integrated/table.service'
import { ActivatedRoute, Router } from '@angular/router'
import { Globals } from '../../globals'
import { PageChangedEvent } from 'ngx-bootstrap/pagination'

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
    providers: [ToslugService, TableService]
})
export class SearchComponent implements OnInit, OnDestroy {
    private connect

    public width: number = 0

    public show

    public categories: any = []

    public dataProduct = []

    public cwstable = new TableService()

    public token: any = {
        getdata: 'api/search'
    }

    public collapsedMenu = []
    openMobileFilter = false

    constructor(private route: ActivatedRoute, public globals: Globals, private router: Router, private toSlug: ToslugService) {
        this.width = document.body.getBoundingClientRect().width

        this.cwstable._ini({
            data: [],
            keyword: 'getdataProductSearch',
            count: this.Pagination.itemsPerPage,
            sorting: { field: 'maker_date', sort: 'DESC', type: 'date' }
        })

        this.cwstable.sorting.field = ''

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getdata':
                    this.search.show = res.data.length > 0 ? 1 : 0
                    this.cwstable._concat(res.data, true)

                    break

                case 'productCategory':
                    this.categories = res.data
                    break

                case 'getProductNew':
                    this.product.new = res.data
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.cwstable._ini({
            data: [],
            cols: [{ title: 'name', field: 'name', show: true, filter: true }],
            keyword: 'getTemplateSearch',
            count: this.Pagination.itemsPerPage,
            sorting: { field: 'name', sort: '', type: 'number' }
        })

        this.product.send()

        this.route.params.subscribe(params => {
            this.search.nameSearch = params.keywords || ''

            if (params.keywords && params.keywords != '') {
                this.globals.send({ path: this.token.getdata, token: 'getdata', params: { keywords: params.keywords } })
            }
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    public product = {
        token: {
            category: 'api/productCategory'
        },
        new: <any>{},
        send: () => {
            this.globals.send({ path: this.product.token.category, token: 'productCategory' })
        }
    }

    public search = {
        value: '',

        nameSearch: '',

        category: { id: 0, name: '', link: '' },

        idGroup: 0,

        show: -1,

        searchCategory: item => {
            this.search.category = item
            this.router.navigate(['/kho-giao-dien/' + this.search.category.link])
        },

        searchValue: () => {
            if (this.search.value != '') {
                this.router.navigate(['/tim-kiem/' + this.toSlug._ini(this.search.value)])
                this.search.value = ''
                document.getElementById('searchProduct').blur()
            }
        }
    }
    searchAgain() {
        if (this.search.value != '') {
            this.router.navigate(['/tim-kiem/' + this.toSlug._ini(this.search.value)])

            this.search.value = ''

            document.getElementById('searchAgain').blur()
        }
    }

    scrollTo(className: string): void {
        const elementList = document.querySelectorAll('.' + className)

        const element = elementList[0] as HTMLElement

        element.scrollIntoView({ behavior: 'smooth' })
    }

    public Pagination = {
        maxSize: 5,

        itemsPerPage: 12,

        change: (event: PageChangedEvent) => {
            const startItem = (event.page - 1) * event.itemsPerPage

            const endItem = event.page * event.itemsPerPage

            this.cwstable.data = this.cwstable.dataList.slice(startItem, endItem)

            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            })
        }
    }
}
